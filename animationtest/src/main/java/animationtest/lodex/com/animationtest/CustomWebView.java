package animationtest.lodex.com.animationtest;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class CustomWebView extends WebView implements Runnable {
    Context context;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    public CustomWebView(Context context) {

        super(context);

    }

    public CustomWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
//        initCustomView("file:///android_asset/index.html");
        Log.d("lodexapp", "CustomWebView: 2 params");
        init(attrs);
    }

    public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Log.d("lodexapp", "CustomWebView: 3 params");

    }

    /*public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }*/

    /*public CustomWebView(Context context, AttributeSet attrs, int defStyleAttr, boolean privateBrowsing) {
        super(context, attrs, defStyleAttr, privateBrowsing);
    }*/

    @Override
    public void run() {

    }

    private void init(@Nullable AttributeSet set) {
        if (set == null) {
            return;
        }

        Log.d("lodexapp", "init: set values");

        TypedArray ta = null;
        String filePath = null;
        try {
            ta = getContext().obtainStyledAttributes(set, R.styleable.MyCustomView);
            filePath = ta.getString(R.styleable.MyCustomView_file_path);
        } catch (Exception e){
            e.printStackTrace();

        }


        if (TextUtils.isEmpty(filePath)) {
            Toast.makeText(getContext(), "هات الملف وتعالى", Toast.LENGTH_SHORT).show();
        }
        else {
            Log.d("lodexapp", "init: " + filePath);
            initCustomView(filePath);
        }

        if (ta != null) {
            ta.recycle();
        }

    }



    public void initCustomView(String embed_link) {
        loadUrl(embed_link);
        setWebChromeClient(new WebChromeClient());
        getSettings().setDomStorageEnabled(true);
        getSettings().setJavaScriptEnabled(true);
        setBackgroundColor(Color.TRANSPARENT);
        setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        setWebViewClient(new WebViewClient() {


            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String requestUrl) {

                Log.i("loading", "" + requestUrl);
                view.loadUrl(requestUrl);
                return true;
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest req) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                shouldOverrideUrlLoading(view, req.getUrl().toString());
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap facIcon) {
                Log.i("started", "" + url);

            }

            public void onPageFinished(WebView view, String url) {
                // ////Log.i("TAG", "Finished loading URL: " +url);
                Log.i("finished", "" + url);

//                if (progressUtils != null && url.equals(embed_link)) {
//                    progressUtils.dismiss_dialog();
//                    progressUtils = null;
//                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e("TAG", "Error: " + description);
                //Log.i("error", "" + failingUrl);

//                if (progressUtils != null) {
//                    progressUtils.dismiss_dialog();
//                    progressUtils = null;
//                    Toast.makeText(PaymentActivity.this, " Something went wrong", Toast.LENGTH_SHORT).show();
//                }
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

    }


}
