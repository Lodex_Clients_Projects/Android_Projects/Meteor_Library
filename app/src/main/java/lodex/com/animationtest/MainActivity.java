package lodex.com.animationtest;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public CustomWebView webView;
    String embed_link;
    Button iClick;
    JavaScriptReceiver javaScriptReceiver;

    @SuppressLint("AddJavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        iClick = findViewById(R.id.clickthis);
        iClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(MainActivity.this, "HI", Toast.LENGTH_SHORT).show();
                webView.initCustomView(embed_link);

            }
        });
        webView = findViewById(R.id.webView);
        embed_link = "file:///android_asset/index.html";
        /*javaScriptReceiver= new JavaScriptReceiver(this);
        if (TextUtils.isEmpty(embed_link))
            onBackPressed();

        javaScriptReceiver = new JavaScriptReceiver(this);
//        webView.addJavascriptInterface(javaScriptReceiver, "JSReceiver");
        webView.loadUrl(embed_link);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        webView.setWebViewClient(new WebViewClient() {


            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String requestUrl) {

                Log.i("loading", "" + requestUrl);
                view.loadUrl(requestUrl);
                return true;
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest req) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                shouldOverrideUrlLoading(view, req.getUrl().toString());
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap facIcon) {
                Log.i("started", "" + url);

            }

            public void onPageFinished(WebView view, String url) {
                // ////Log.i("TAG", "Finished loading URL: " +url);
                Log.i("finished", "" + url);

//                if (progressUtils != null && url.equals(embed_link)) {
//                    progressUtils.dismiss_dialog();
//                    progressUtils = null;
//                }
            }

            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e("TAG", "Error: " + description);
                //Log.i("error", "" + failingUrl);

//                if (progressUtils != null) {
//                    progressUtils.dismiss_dialog();
//                    progressUtils = null;
//                    Toast.makeText(PaymentActivity.this, " Something went wrong", Toast.LENGTH_SHORT).show();
//                }
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });




*/










//
//        webView.setWebChromeClient(new WebChromeClient());
//        webView.getSettings().setDomStorageEnabled(true);
//        webView.getSettings().setJavaScriptEnabled(true);
//
//        webView.setBackgroundColor(Color.TRANSPARENT);
//        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);




//        webView.setWebViewClient(new WebViewClient() {
//
//
//
//            @SuppressWarnings("deprecation")
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String requestUrl) {
//
//                Log.i("loading", "" + requestUrl);
//                view.loadUrl(requestUrl);
//                return true;
//            }
//
//            @TargetApi(android.os.Build.VERSION_CODES.M)
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest req) {
//                // Redirect to deprecated method, so you can use it in all SDK versions
//                shouldOverrideUrlLoading(view, req.getUrl().toString());
//                return true;
//            }
//
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap facIcon) {
//                Log.i("started", "" + url);
//
//            }
//
//            public void onPageFinished(WebView view, String url) {
//                // ////Log.i("TAG", "Finished loading URL: " +url);
//                Log.i("finished", "" + url);
//
////                if (progressUtils != null && url.equals(embed_link)) {
////                    progressUtils.dismiss_dialog();
////                    progressUtils = null;
////                }
//            }
//
//            @SuppressWarnings("deprecation")
//            @Override
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Log.e("TAG", "Error: " + description);
//                //Log.i("error", "" + failingUrl);
//
////                if (progressUtils != null) {
////                    progressUtils.dismiss_dialog();
////                    progressUtils = null;
////                    Toast.makeText(PaymentActivity.this, " Something went wrong", Toast.LENGTH_SHORT).show();
////                }
//            }
//
//            @TargetApi(android.os.Build.VERSION_CODES.M)
//            @Override
//            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
//                // Redirect to deprecated method, so you can use it in all SDK versions
//                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
//            }
//        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        webView.onResume();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the state of the WebView
        webView.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore the state of the WebView
        webView.restoreState(savedInstanceState);
    }
}
